# Laser Cut Magnetic Tube Racks

## Purpose

We have a parametric 3D printed design for magnetic rack holders but it has the following drawbacks:
- 3D printing is slow, several hours for a large holder and speeding up assembly requires many 3D printers
- The design uses a lot of plastic
- You have to remove all the tubes by hand to go to shake them/vortex them
- If you don't have a really good 3D printer the final rack can have defects and look unprofessional

This project overcomes some of these constraints by:
 - Using a laser cut design
 - Ensuring the design is useable with minimal modification for a range of tube sizes
 - reducing the number of magnets used


## Materials

### Acrylic

- 3mm for base
- 5 mm acryclic for tube holder and tube holder slot
- 10 mm acrylic for magnet holder

### Magnets

Initial test: [Guy's Magnets 20 mm x 15 mm x 5 mm N52 neodymium block](https://www.guysmagnets.com/neodymium-magnets-c11/guys-magnets-20-mm-x-15-mm-x-5-mm-n52-neodymium-block-p1080)


